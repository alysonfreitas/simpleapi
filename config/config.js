module.exports = {
    dev: {
        message: 'service is running in DEV mode',
        port: 8081,
        api: {
            resolve: function (endpoint) {
                return this.prefix + endpoint;
            },
            prefix: '/api/v1'
        },
        database: {
            username: 'root',
            password: '',
            database: 'studydb',
            host: '127.0.0.1',
            dialect: 'mysql',
            force: false
        }
    },
    prod: {
        message: 'service is running in PRODUCTION mode',
        port: 8081,
        api: {
            resolve: function (endpoint) {
                return this.prefix + endpoint;
            },
            prefix: '/api/v1'
        },
        database: {
            username: 'root',
            password: '',
            database: 'studydb',
            host: '127.0.0.1',
            dialect: 'mysql',
            force: false
        }
    }
}