const env = process.env.config || 'dev';
const config = require('./config/config')[env];
const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const glob = require('glob');
const path = require('path');
require('./entities')(config);

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static('public'));

glob.sync('./routes/*.js').forEach(function (file) {
    require(path.resolve(file))(app, config);
});

var server = app.listen(config.port, function () {
    var host = server.address().address
    var port = server.address().port
    console.log(config.message);
    console.log("listening at http://%s:%s", host, port)
})