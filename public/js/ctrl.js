var app = angular.module('app', []);
app.controller('ctrl', function ($scope, $http) {
    $scope.people = [];

    $scope.init = function () {
        $http({
            method: 'GET',
            url: '/api/v1/person'
        }).then(function successCallback(response) {
            $scope.people = response.data;
        }, function errorCallback(response) {
            console.error('unable to connect to api');
        });
    }
});