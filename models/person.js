module.exports = {
    getAll: async function () {
        return await global.entities.person.findAll({ raw: true });
    },
    filtered: async function (filter) {
        var persons = await this.getAll();
        if (filter.age != null) {
            persons = persons.filter(person => person.age == filter.age);
        }
        if (filter.name != null) {
            persons = persons.filter(person => person.name.indexOf(filter.name) > -1);
        }
        if (filter.id != null) {
            persons = persons.filter(person => person.id == filter.id);
        }
        return persons;
    },
    get: async function (id) {
        const persons = await this.getAll();
        const person = persons.find(person => person.id == id);
        return person;
    }
} 