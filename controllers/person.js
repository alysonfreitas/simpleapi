var model = require("../models/person")

module.exports = {
    get: async function (req, res) {
        var id = req.params.id;
        var person = await model.get(id);
        res.send(person);
    },
    getAll: async function (req, res) {
        var persons = await model.getAll();
        res.send(persons);
    },
    filtered: async function (req, res) {
        var filter = {
            id: req.body.id,
            name: req.body.name,
            age: req.body.age
        }
        var persons = await model.filtered(filter);
        res.send(persons);
    }
}