module.exports = function (Sequelize, db) {
    const key = 'person';
    const config = {
        freezeTableName: true
    };
    const props = {
        name: Sequelize.STRING,
        age: Sequelize.INTEGER
    };
    const entity = db.define('Person', props, config);
    return { key, entity };
}

