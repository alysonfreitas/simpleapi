var controller = require("../controllers/person")

module.exports = function (app, config) {
    app.get(config.api.resolve('/person'), function (req, res) {
        controller.getAll(req, res);
    });
    app.get(config.api.resolve('/person/:id'), function (req, res) {
        controller.get(req, res);
    });
    app.post(config.api.resolve('/person'), function (req, res) {
        controller.filtered(req, res);
    });
}

