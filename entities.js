const glob = require('glob');
const path = require('path');
const Sequelize = require('sequelize');

module.exports = function (config) {
    var db = new Sequelize(config.database);
    const files = glob.sync('./entities/*.js');
    db.entities = {};
    for (var i in files) {
        const file = files[i];
        const res = require(path.resolve(file))(Sequelize, db);
        db.entities[res.key] = res.entity;
    }
    global.entities = db.entities;
    db.sync({
        force: config.database.force || false
    });
    return db;
}